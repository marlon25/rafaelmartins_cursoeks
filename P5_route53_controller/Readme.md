**Criação do route53 controller**

 
```
curl -o route53-policy.json https://rafaelmartinscursoeks.s3.amazonaws.com/P5_route53_controller_r53_policy.txt
```

```
aws iam create-policy \
  --policy-name Route53IAMPolicy \
  --policy-document file://route53-policy.json
```

```
export PolicyR53=$(aws iam list-policies --query 'Policies[?PolicyName==`Route53IAMPolicy`].Arn' --output text)
```

```
eksctl create iamserviceaccount \
 --name external-dns \
 --namespace kube-system \
 --cluster $CLUSTERNAME \
 --attach-policy-arn $PolicyR53 \
 --approve
```

```
curl -O "https://rafaelmartinscursoeks.s3.amazonaws.com/route53_deployment.yaml"

# IMPORTANTE!! ABRIR O ARQUIVO "route53_deployment.yaml" e substituir a linha -domain-filter=eks.yourdomain.cloud, pelo seu Domínio, publico ou privado
 
kubectl apply -f route53_deployment.yaml
```



***Teste***

```
kubectl logs -n kube-system $(kubectl get po -n kube-system | egrep -o external-dns[a-zA-Z0-9-]+)
```






