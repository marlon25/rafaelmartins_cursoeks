**Criação do ingress controller**


```
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/aws-alb-ingress-controller/v1.1.9/docs/examples/rbac-role.yaml
```


```
aws ec2 create-tags --resources subnet-id_a_xxxxx subnet-id_c_xxxxx --tags Key="kubernetes.io/role/internal-elb",Value=1
```

***(subnets-xxxx usadas na criação do cluster)***
***Atentar a tag kubernetes.io/role/internal-elb se cluster for privado.***

***kubernetes.io/role/elb para cluster publico***

https://docs.aws.amazon.com/eks/latest/userguide/alb-ingress.html

```
eksctl utils associate-iam-oidc-provider --cluster=$CLUSTERNAME --approve
```

```
eksctl create iamserviceaccount --cluster=$CLUSTERNAME --namespace=kube-system --name=alb-ingress-controller --attach-policy-arn=$PolicyARN --override-existing-serviceaccounts --approve
```

```
kubectl apply -k github.com/aws/eks-charts/stable/aws-load-balancer-controller/crds?ref=master
```

```
helm repo add eks https://aws.github.io/eks-charts

helm upgrade -i aws-load-balancer-controller \
    eks/aws-load-balancer-controller \
    -n kube-system \
    --set clusterName="${CLUSTERNAME}" \
    --set serviceAccount.create=false \
    --set serviceAccount.name=aws-load-balancer-controller \
    --set image.tag="${LBC_VERSION}"

kubectl -n kube-system rollout status deployment aws-load-balancer-controller

```

***Ref: https://www.eksworkshop.com/beginner/130_exposing-service/ingress_controller_alb/***


***Teste***

```
kubectl logs -n kube-system $(kubectl get po -n kube-system | egrep -o alb-ingress[a-zA-Z0-9-]+)
```






